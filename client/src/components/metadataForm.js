import { TextField, Datagrid, List, DateField,ImageField, UrlField} from 'react-admin'
import * as React from 'react'
export const metadataForm = props => {
   return (
       <List {...props}>
           <Datagrid>
               <TextField source='id' />
               <TextField source='nft_name' />
               <TextField source='nft_address' />
               <TextField source='nft_account' />
               <TextField source='owner' />
               <TextField source='holder_account' />
               <TextField source='signature' />
               <TextField source='distribution' />
               <ImageField source='nft_img_url' />
               <UrlField source='nft_json_url' />
             

               {/* <DateField source='due' /> */}
           </Datagrid>
       </List>
   )
}
