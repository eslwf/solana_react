import * as React from 'react';
import { DashboardMenuItem, Menu, MenuItemLink } from 'react-admin';
import BookIcon from '@material-ui/icons/Book';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import PeopleIcon from '@material-ui/icons/People';
import LabelIcon from '@material-ui/icons/Label';

export const MenuBar = (props) => (
    <Menu {...props}>
        <DashboardMenuItem to="/"/>
        {/* <DashboardMenuItem to="/home"/>  */}
        {/* <MenuItemLink to="/home" primaryText="HomePage" leftIcon={<BookIcon />}/> */}
        {/* <MenuItemLink to="/comments" primaryText="Comments" leftIcon={<ChatBubbleIcon />}/>
        <MenuItemLink to="/users" primaryText="Users" leftIcon={<PeopleIcon />}/>
        <MenuItemLink to="/custom-route" primaryText="Miscellaneous" leftIcon={<LabelIcon />}/> */}
    </Menu>
);