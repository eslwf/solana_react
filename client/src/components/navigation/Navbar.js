import React from 'react';

export default function Navbar() {
  return(
        <nav className="navbar">
            <h1>Developer Mode</h1>
            <div className="links">
                <a href="/">Home</a>
                <a href="/profile">Profile</a>
                <a href="/database">Database</a>
            </div>
        </nav>

  );
}
