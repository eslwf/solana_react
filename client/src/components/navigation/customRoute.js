
import React,{Fragment} from 'react';
import { Route } from 'react-router-dom';
import { Admin, ListGuesser, Resource, RouteWithoutLayout } from 'react-admin';
import {Homepage} from "../homepage/Homepage.js";
import {metadataForm} from "../metadataForm" 
import Profile from '../profile/Nft_profile.js';
import postgrestRestProvider from '@raphiniert/ra-data-postgrest';
import Foo from '../homepage/NFT_Card/test/foo.js';
import { Card_React } from '../homepage/NFT_Card/admin-card.js';
 

export default [
    <Route exact path="/home" component={Homepage} />,
    <RouteWithoutLayout  path="/profile"  component={Foo}/>, // ${resource}/${record.id}/show   render={() =>  <Foo />}
];


// export default function PageRouter(props) {

//     return (
//           <Router> {/*Router is the global route while Routes is local */}
             
//               <Switch>
//                   <Route exact path="/"><Homepage /></Route>
//                   <Route path="/profile"><Profile />
//                   </Route> 
//                   <Route exact path="/database">
//                       <Admin dataProvider={dataProvider} >
//                       <Resource name="metadata" list={metadataForm} />
  
//                   </Admin>  
//                   </Route> 
                 
//               </Switch>
              
//           </Router>
       
//     );
//   }
  