import {  Datagrid, List, DateField,ImageField, UrlField,ShowButton, TopToolbar,ListButton, TextInput} from 'react-admin'
import * as React from 'react'
import {Button,Typography,Grid,Container,
  Card as MuiCard , CardMedia,CardContent,CardActions, withStyles
    } from "@material-ui/core"
import { useRecordContext } from 'react-admin';
import { Link } from 'react-router-dom';
import { linkToRecord, Record, useResourceContext } from 'ra-core';
import { memo, useMemo, ReactElement } from 'react';
import ImageEye from '@material-ui/icons/RemoveRedEye';
import {FilterBar} from './filter/FilterBar';
import SearchBar from './filter/SearchBar';
// const dataProvider = postgrestRestProvider('http://172.104.42.214:5005');
// <Admin  dataProvider={dataProvider}  customRoutes={customRoute}>
//       <Resource name="attr" list={Homepage} show={ProfileShow}/>
// </Admin> 

const MoreInfo = (props) => { 

  const { source } = props;
  const record = useRecordContext(props);
  
  // <ShowButton actions={<TestActions />}{...props}></ShowButton>;
  return <ShowButton  label="More Info"record={record}></ShowButton>;
}

const MyUrlField = ({ source }) => {
  const record = useRecordContext(); 
  // console.log("Record: ", record)
  return record ? (
      <a href={record[source]}>
          {record[source]}
      </a>
  ) : null;
}

export default MyUrlField;

const TextField = ({ source }) => { //source = string : nft_addres
  const record = useRecordContext();
  /*
  var obj = {id : 1}, 
  var id ="id"
  console.log(obj[id]) */
 
  return (<span style={{ color: 'black' }}>{source}: {record && record[source]}</span>);
};

//

export const Homepage = (props)=>{
 
  return (
    <List {...props} aside={<FilterBar />} filters={SearchBar} >  
        <Datagrid>
        <ImageField source='nft_img_url' label= 'NFT_Image' title='NFT_Image' />
        <div className="Container" align="center">
            <TextField source="id"/>
            <br />
            <TextField source="nft_name"/>
            <br />
            <TextField source="nft_address" />
            <br />
            <UrlField source ='nft_img_url' />
            <br />
            <MyUrlField source="nft_json_url" />
        </div>
        <MoreInfo />
      </Datagrid>
          

    </List>
  )
}

