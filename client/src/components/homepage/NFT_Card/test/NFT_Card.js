import * as React from 'react';
import { useEffect, useState } from 'react'
import { useDataProvider } from 'react-admin';
import {Button,Typography,Grid,Container,
        Card,CardMedia,CardContent,CardActions
        } from "@material-ui/core"
import postgrestRestProvider from '@raphiniert/ra-data-postgrest';

// const dataProvider = postgrestRestProvider('http://172.104.42.214:5005');


export default function NFT_Card(){
  const [data,setData] = useState([])
 
    async function getData(){
        const res = await fetch('http://172.104.42.214:5005/metadata')
        const d = await res.json()
        setData(d)
    }
      useEffect(()=>{
          getData()
      },[])// to ensure only make one request
    
  
  console.log(data)
  
    return(
      <section class="mt-5">
      <div class="row">
          {data.map(d=>(
          <div class="col-3">
              <div class="card" width="18rem;">
            
      
                  <img class="card-img-top" id ={d.id} src={d.nft_img_url} class="img-fluid" alt="Card image cap" />
                  <div class="card-body">
                      <h5 class="card-title">{d.nft_name}</h5>
                      <ul class="card-title font popping">Address: <a href ={`https://solscan.io/token/${d.nft_address}`} target="_blank">{d.nft_address}</a></ul>
                      <a href={d.nft_json_url} class="btn btn-primary">NFT JSON</a>
                  </div>
              </div>   
          </div>
      ))}
      
      </div>
</section>

    )
}

