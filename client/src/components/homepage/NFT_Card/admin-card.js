import {  Datagrid, List, DateField,ImageField, UrlField,ShowButton} from 'react-admin'
import * as React from 'react'
import {Button,Typography,Grid,Container,
    Card,CardMedia,CardContent,CardActions
    } from "@material-ui/core"
import { useRecordContext } from 'react-admin';
import { Link } from 'react-router-dom';
import { linkToRecord, Record, useResourceContext } from 'ra-core';
import MyUrlField from './test/test';
import { memo, useMemo, ReactElement } from 'react';
import ImageEye from '@material-ui/icons/RemoveRedEye';

const TestButton = (props) => { 
    
/*props :
{basePath: '/profile', children: 'View More', record: {…}, resource: 'metadata'}
basePath: "/profile"
children: "View More"
record: {id: 22, nft_name: 'SolChan #893', nft_address: 'CNSyDGwjH4YdfhTiYmcPfsZseRaDqZspdBA1paQj7aCv', nft_account: 'CwKy65beeYJV7Yi8Ec3DeZEh1Ys8JqrPJzQHWJWxHXUn', owner: 'DBBNM3QrLq1RSr5ZBAA81Nx3TthwhWVYKWaAmQniY3mo', …}
resource: "metadata"
[[Prototype]]: Object */
    const { source } = props;
    const record = useRecordContext(props);
    console.log(record)
    return <ShowButton record={record}>View More</ShowButton>;
/*record:
distribution: 1
holder_account: "HxxPVzCXurFNpNNRGt1ZsHtUxD5k2y5iTZaCSmVjeHY9"
id: 22
nft_account: "CwKy65beeYJV7Yi8Ec3DeZEh1Ys8JqrPJzQHWJWxHXUn"
nft_address: "CNSyDGwjH4YdfhTiYmcPfsZseRaDqZspdBA1paQj7aCv"
nft_img_url: "https://v1-meta.solchan.io/893.jpg"
nft_json_url: "https://v1-meta.solchan.io/893.json"
nft_name: "SolChan #893"
owner: "DBBNM3QrLq1RSr5ZBAA81Nx3TthwhWVYKWaAmQniY3mo"
signature: "3rqfczkYfuS7oB5uE6RJLfLWNNxXdV616rC3r5HfFPgP3MrseiUEHpvotB36HvcEepdQSuS3aEQ4HJ5w7AnAC6Cq"
[[Prototype]]: Object
 */
// const defaultIcon = <ImageEye />;
//     const {
//         basePath = '',
//         icon = defaultIcon,
//         record,
//         scrollToTop = true,
//         ...rest
//     } = props;
//     /*console.log(`${linkToRecord(
//         basePath || `/${resource}`,
//         record.id
//     )}/show`)*/  // `/metadata/10/show`
//     const resource = useResourceContext();
//     return (<Button component={Link}
//     to={useMemo(
//         () => ({
//             pathname: record
//                 ? `${linkToRecord(
//                     basePath || `/${resource}`,
//                     record.id
//                 )}`
//                 : '',
//             state: { _scrollToTop: scrollToTop },
//         }),
//         [basePath, record, resource, scrollToTop]
//     )}>{icon}</Button>)
// }
}

const TextField = ({ source }) => { //source = string : nft_addres
    const record = useRecordContext();
    /*
    var obj = {id : 1}, 
    var id ="id"
    console.log(obj[id]) */
   
    return (<span style={{ color: 'black' }}>{source}: {record && record[source]}</span>);
};
export const Card_React = props => {
    
   return (
        <List {...props}>
            <Datagrid>

            <ImageField source='nft_img_url' label= 'NFT_Image' title='NFT_Image' />
            <div className="Container" align="center">
                <TextField source="id"/>
                <br />
                <TextField source="nft_name"/>
                <br />
                <TextField source="nft_address" />
                <br />
                <UrlField source ='nft_img_url' />
                <br />
                {/* <UrlField source='nft_json_url' />
                <br /> */}
                <MyUrlField source="nft_json_url" />
            </div>
            <TestButton />
            

        </Datagrid>
             
    
       </List>
   )
}


