import {TextInput, SearchInput} from 'react-admin'
export default [
    <TextInput label="Search ID" source="id"  resettable alwaysOn />,
    <TextInput label="NFT Name" source="nft_name" />,
    <TextInput label="NFT Address" source="nft_address" />
];