// Side Bar //

import React from 'react';
import { Card as MuiCard, CardContent, withStyles } from '@material-ui/core';
import { List,FilterList, FilterListItem,TextInput } from 'react-admin'
import { useRecordContext } from 'react-admin';

const HasNewsletterFilter = () => (
  <FilterList label="Has newsletter">
  <FilterListItem
      label="True"
      value={{ has_newsletter: true }}  // column metadata.has_newsletter does not exist
  />
  <FilterListItem
      label="False"
      value={{ has_newsletter: false }}
  />
</FilterList>
)


// const getResourceProvider = ()=>{
//   var test = await dataProvider.getList("attr");

// }

const BodyColor = () => (
  
  <FilterList label="Body Color">
  {/* <FilterListItem
      label="True"
      value={{ trait_type: true }}  // column metadata.has_newsletter does not exist
  />
  <FilterListItem
      label="False"
      value={{ trait_type: false }}
  /> */}
</FilterList>
)





export const Card = withStyles(theme => ({
  root: {
      [theme.breakpoints.up('sm')]: {
          order: -1, // display on the left rather than on the right of the list
          width: '15em',
          marginRight: '1em',
      },
      [theme.breakpoints.down('sm')]: {
          display: 'none',
      },
  },
}))(MuiCard)

const TextField = ({ source }) => { //source = string : nft_addres
  const record = useRecordContext();
  /*
  var obj = {id : 1}, 
  var id ="id"
  console.log(obj[id]) */
 
  return (<span style={{ color: 'black' }}>{source}: {record && record[source]}</span>);
};

export const FilterBar = () => {
  return (
    <Card>
      <CardContent>
        <h1>Filter Bar</h1>
        {/* <TextField source="nft_addr" /> */}
        {/* <HasNewsletterFilter /> */}
        <BodyColor />
      </CardContent>
    </Card>
  );
}
