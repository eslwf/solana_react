import * as React from "react";

import {
    Chip

} from "@material-ui/core"
import { useRecordContext } from 'react-admin';
export const History= (props) => {
    const record = useRecordContext(props); 
    return (
      <div>
           <Chip  size='medium' label={` Owner : ${record.owner}`} variant="outlined" />
           <br />
    
           <Chip  size='medium' label={` Signature : ${record.signature}`} variant="outlined" />
      </div>
    
    )
}