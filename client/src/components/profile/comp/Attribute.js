import * as React from "react";
import { useEffect, useState } from 'react';
import {
    Card, CardMedia, CircularProgress, Box, Chip

} from "@material-ui/core"
import { useRecordContext } from 'react-admin';


const Attr= ({results})=>{
    const data = results
    const arr = data.attributes
    return (
      <div>
        {arr.map((row) => (
        <Chip  size='medium' label={`${row.trait_type} : ${row.value}`} variant="outlined" />
        ))}
      </div>
    )
  }
  
  
  
  
  function CircularIndeterminate() {
    return (
      <Box sx={{ display: 'flex' }}>
        <CircularProgress />
      </Box>
    );
  }
  
  // const AttributesTable = ({results})=>{
  //     const data = results
  //     const arr = data.attributes
  //     return(
  
  //             <TableContainer component={Paper}>
  //             <Table sx={{ minWidth: 650 }} aria-label="simple table">
  //                 <TableHead>
  //                 <TableRow>
  //                     <TableCell>Trait Type</TableCell>
  //                     <TableCell align="center">Value</TableCell>
  //                 </TableRow>
  //                 </TableHead>
  //                 <TableBody>
  
  //                 {arr.map((row) => (
  //                     <TableRow
  //                     key={row.trait_type}
  //                     sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
  //                     >
  //                     <TableCell component="th" scope="row">
  //                         {row.trait_type}
  //                     </TableCell>
  //                     <TableCell align="center">{row.value}</TableCell>
  //                     </TableRow>
  //                 ))}
  //                 </TableBody>
  //             </Table>
  //             </TableContainer>
              
              
          
         
  //     )
  // }
  
  
  
  
export const Attribute = (props) => {
      const record = useRecordContext(props); 
      const img_url = record.nft_img_url
      const json_url = record.nft_json_url
      const [loading,setLoading]  = useState(true);
      const [data,setData] = useState([])
          async function getData(){
              const res = await fetch(json_url)
              const d = await res.json()
              setData(d)
              setLoading(false)
          }
  
      useEffect(()=>{
          getData()
      },[])
      // console.log("Parent Loading" , loading, "Data", data)
          
  
    return (
        <div>
              <div> 
                  <Card sx={{ maxWidth: 345 }}>
                  <CardMedia
                      component="img"
                      alt={record.nft_name}
                      image={img_url}
                  />
                  </Card>
                  <br />
                  {loading ? <CircularIndeterminate /> : <Attr results={data} />}
  
              </div>
  
              
      </div>
    );
  }