
import * as React from "react";
import { Show, SimpleShowLayout } from 'react-admin';

import { History } from "./comp/History";
import { Attribute } from "./comp/Attribute";



export const ProfileShow = (props) => {
  
    return(
    <Show {...props}>
        <SimpleShowLayout>
            
   
            <Attribute />
            <br /> 
            <History />
        </SimpleShowLayout>
    </Show>
    )
}
