import React, {Fragment} from "react";
import { Admin, ListGuesser, Resource, FilterList , useResourceContext, ResourceContextProvider} from 'react-admin';
import postgrestRestProvider from '@raphiniert/ra-data-postgrest';
// import {metadataForm} from "./components/metadataForm" 
import customRoute from "./components/navigation/customRoute";
import Navbar from "./components/navigation/Navbar";
import { BrowserRouter as Router,Routes, Route, Link } from 'react-router-dom';
import {Homepage} from "./components/homepage/Homepage";
import Profile from "./components/profile/Nft_profile";
import { metadataForm } from "./components/metadataForm";
import Foo from "./components/homepage/NFT_Card/test/foo";
import { Card_React } from "./components/homepage/NFT_Card/admin-card";
import { Framework } from "./components/Layout";
import jsonServerProvider from 'ra-data-json-server';
import {ProfileShow} from "./components/profile/Nft_profile"
import { FilterBar } from "./components/homepage/filter/FilterBar";
import {Card} from "./components/homepage/filter/FilterBar"

const dataProvider = postgrestRestProvider('http://172.104.42.214:5005');

function App() {    
    return (
        <Fragment>
          
             {/* <div>
                <Navbar />
            </div> */}
            <Admin layout={Framework} dataProvider={dataProvider}  customRoutes={customRoute}>
                <Resource name="metadata" list={Homepage} show={ProfileShow}/>
                {/* <Resource name="attr" Card={FilterBar}/> */}
            </Admin> 
            {/* <PageRouter /> */}
           
        </Fragment>)
}
 
export default App;




