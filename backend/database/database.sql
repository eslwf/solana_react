CREATE DATABASE nft_database;
--\c NFT_Data
create schema nft_schema;
CREATE TABLE nft_schema.metadata(
    id SERIAL,
    nft_name VARCHAR(255) not NULL,
    nft_address VARCHAR(255) Primary key not NULL ,
    nft_account VARCHAR(255),
    owner VARCHAR(255),
    distribution INTEGER,
    nft_img_url TEXT,
    nft_json_url TEXT,
    signature VARCHAR(255),
    holder_account VARCHAR(255)
);


-- nft_role  

-- Granting Select privilege:
-- grant usage on schema nft_schema to nft_role;
-- grant select on nft_schema.metadata to nft_role;

-- To grant write privilege:
-- grant all privileges on schema nft_schema to nft_role;
-- grant all privileges on all sequences in schema nft_schema to nft_role;

--create role nft_auth_role noinherit login password 'testadmin123';

-- nft_user testadmin123
-- grant nft_role to nft_user;
-- grant nft_auth_role to nft_user;

