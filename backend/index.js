const express = require("express")
const app = express()
const pool = require("./database/db")
const cors = require("cors")
app.use(express.json()) //=>req.body
app.use(cors())
app.listen("4211",()=>{console.log(`Successfully connected port=4211`)})


//multiple add data



async function postOne(res,sent_data){
    try{
        const newMetadata = await pool.query(
            `INSERT INTO nft_schema.metadata (
                nft_name,
                nft_address,
                nft_account,
                owner,
                distribution,
                nft_img_url,
                nft_json_url,
                signature,
                holder_account) 
            VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING *`,
            [
            sent_data.nft_name,
            sent_data.nft_addr,
            sent_data.token_account,
            sent_data.owner,
            sent_data.distribution,
            sent_data.img_url,
            sent_data.json_url,
            sent_data.signature,
            sent_data.holder_account]
        )
        res.json(newMetadata.rows[0])
    }
    catch(err){
        console.error(err.message)
    }
}
app.post("/metadata",async(req,res)=>{
    const sent_data = req.body;
    for(var i =0;i<Object(sent_data).length;i++){
        console.log("element", i , sent_data[i])
        postOne(res,sent_data[i])
    }
})

app.get("/metadata",async(req,res)=>{
    const allData = await pool.query(
        "SELECT * FROM nft_schema.metadata"
    )
    res.json(allData.rows)
})
/*Get one NFT */
app.get("/metadata/:nft_address",async(req,res)=>{
    const {nft_address} = req.params
    console.log("GET nft_address",nft_address)
    const nft_item = await pool.query(
        "SELECT * FROM nft_schema.metadata WHERE nft_address=$1",
        [nft_address])
    res.json(nft_item.rows[0])

})


app.delete("/metadata/:nft_address",async(req,res)=>{
    const {nft_address} = req.params
    console.log("Deleted nft_address:", nft_address)
    const deleteNFT = await pool.query(
        "DELETE FROM nft_schema.metadata WHERE nft_address=$1",
        [nft_address])
    res.json(`Deleted nft_address : ${nft_address}`)
})

async function postOneAttr(res,nft_addr, trait_type, value){
    try{
        const inserted_data = await pool.query(
            `INSERT INTO attr_schema.attr_table (
                nft_addr,
                trait_type,
                value)
                VALUES($1,$2,$3) RETURNING *`,
                ([nft_addr,trait_type,value])

        )
        res.json(inserted_data.rows[0])
        console.log("After Inserted ", inserted_data.rows[0])
    }
    catch(err){
        console.error(err.message)
    }
}
/*
    "trait_type": "{\"Body Color\",\"Background\",\"Face\",\"Eyes\",\"Hair\",\"Hair Color\",\"Swimsuit\",\"Swimsuit Color\",\"Accessory 3\"}",
    "value": "{\"Medium Tan\",\"Tropical\",\"Sly\",\"Green\",\"Braid\",\"Fair-haired\",\"Cross\",\"Green\",\"Black choker\"}"
 */
app.post("/attr",async(req,res)=>{
    try{
        const sent_data = req.body;
        
        
        for(var i =0;i<Object(sent_data).length;i++){
           
            postOneAttr(res,sent_data[i]["nft_addr"],sent_data[i]["trait_type"], sent_data[i]["value"])
        }
    }
    catch(err){
        console.log("POST ARR", err.message)
        res.json(sent_data)
    }
    // res.json(sent_data)
})
app.post("/attr/:nft_address",async(req,res)=>{
    const sent_data = req.body;
 
    try{
      
        // console.log((sent_data["trait_type"])) 
        postOneAttr(res,sent_data["nft_addr"],sent_data["trait_type"], sent_data["value"])
        
    }
    catch(err){
        console.log("POST ONE ", err.message)
        res.json(sent_data)
    }
  
})

app.get("/attr",async(req,res)=>{
    try{
        const get_data = await pool.query(
            "SELECT * FROM attr_schema.attr_table"
        )
        res.json(get_data.rows)
    }
    catch(err){
        console.log("GET ", err.message)
    }
})

app.delete("/attr/:nft_address",async(req,res)=>{
    const {nft_address} = req.params
    console.log("Deleted nft_address:", nft_address)
    
    const deleteNFT = await pool.query(
        "DELETE FROM attr_schema.attr_table WHERE nft_addr=$1",
        [nft_address])
    res.json(`Deleted nft_address : ${nft_address}`)
    
   
      
    
})




module.exports = app