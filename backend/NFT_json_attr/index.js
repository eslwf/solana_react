const axios = require("axios")
const fs = require("fs")

async function get_json(json_url){
    try {
        var data_arr = await axios(json_url)
        return data_arr.data
    }
    catch(e){
        console.log(json_url)
    }
}

function trait_value_func (attr){
    var trait_arr = []
    var value_arr = []
    for(var i =0; i < attr.length;i++){
        // console.log("type of in nft_json attr", typeof(attr[i]["trait_type"]))
        trait_arr.push(attr[i]["trait_type"].toString())
        value_arr.push(attr[i]["value"].toString())
    }
    // console.log("trait_arr",trait_arr)
    return [trait_arr,value_arr]
}

async function extract_json_data(json_data_arr){
    let nft_pk_json_url = []
    for(let i = 0; i < json_data_arr.length;i++){
        url = String(json_data_arr[i]["json_url"])
        data = await get_json(url)
       
        try{
            var attr = data["attributes"]
            var trait_arr = trait_value_func(attr)[0]
            var value_arr = trait_value_func(attr)[1]
            let obj = {
                nft_addr : json_data_arr[i]["nft_addr"],
                json_url : url,
                trait_type : trait_arr,
                value: value_arr
            }
            console.log(`object: `, obj)
            nft_pk_json_url.push(obj)
        }
        catch(err){
            console.log("error data " , data)
            console.log(err.message)
        }
    }
    return nft_pk_json_url
}


async function main(){
    let filepath = "../NFTData/testfile/6th.json"
    let rawdata = fs.readFileSync(filepath)
    let json_data_arr = JSON.parse(rawdata)
    data_write = await extract_json_data(json_data_arr)

    fs.writeFileSync("attr.json", JSON.stringify(data_write,null, 2))

}

main()