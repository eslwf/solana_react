


create schema attr_schema;
create table attr_schema.attr_table(
    id serial,
    nft_addr text primary key,
    trait_type text,
    value text
);