const web3 = require('@solana/web3.js');
const bs58 = require('bs58')
const BN = require('bn.js');
const splToken = require('@solana/spl-token');
const { PublicKey, Connection } = require("@solana/web3.js/lib/index.cjs");
const axios = require("axios")
require("dotenv/config")
const mt = require('@metaplex/js');
const { Metadata } = require ("@metaplex-foundation/mpl-token-metadata");

                /* Local Dependencies */
const acc = require("./acc.js")
const tx = require("./tx.js")
var connection = new web3.Connection(web3.clusterApiUrl(process.env.Cluster_Name))

async function getAllTokens(ppl_addr){
    let pk = new web3.PublicKey(ppl_addr)
    let token_accounts_query_results = await connection.getTokenAccountsByOwner(pk,{programId:splToken.TOKEN_PROGRAM_ID})//Token instructions, Token program
    //console.log(token_accounts_query_results.value.forEach(i=>console.log(i.account.owner.toBase58())))
    //console.log(token_accounts_query_results.value.forEach(i=>console.log(i.account.data)))
    
    var token_holding =[]
    //token_accounts_query_results.value.forEach(i=>token_holding_account.push(i.pubkey.toBase58())) // token_account owned by the owner_pk
    token_accounts_query_results.value.forEach(i=>token_holding.push({
        "nft_addr" : new web3.PublicKey(splToken.AccountLayout.decode(i.account.data).mint).toBase58(),
        "token_account":i.pubkey.toBase58()}))//Support for translating between Buffer instances and JavaScript
    
    //console.log(token_accounts_query_results.value.forEach(i=>console.log(new web3.PublicKey(splToken.AccountLayout.decode(i.account.data).mint).toBase58(),i.pubkey.toBase58())))//Support for translating between Buffer instances and JavaScript   
    // let tokens = token_accounts_query_results.value.map(t=>{return new web3.PublicKey(splToken.AccountLayout.decode(t.account.data).mint)}) //originally it is binary
    return token_holding
}


async function holder_helper_function(transaction_data,nft_addr,signature){
    var arr = transaction_data.meta.innerInstructions//.forEach(i=>i.instructions.map(j=>console.log("This is j", j,j.programId.toBase58())))
    var found = false // if break 2nd for loop, it cannot find other 
    var holder = {
        "holder_account": undefined,
        "owner": undefined
        
    }
    if(arr.length == 0){
        console.log("I have no inner instruction",nft_addr)
        var ins_arr = transaction_data.transaction.message.instructions[0]
      
        if(ins_arr.parsed !=undefined){ //parsedInstruction Type
            console.log("ParsedInstruction")
            var data = await tx.getDatafromParsedInstruction(ins_arr.parsed)
            holder.holder_account = data
        }
        else{ // PartiallyDecodedInstruction type
            console.log("PartiallyDecodedInstruction")
            holder.owner = await tx.getDatafromPartiallyDecodedInstruction(ins_arr)
            
        }
        found = true
    }
    for(var i =0; i<arr.length && !found;i++){
        for (var j=0;j<arr[i].instructions.length;j++){
            var transaction_program_id = arr[i].instructions[j].programId.toBase58()
            const token_program_id = "TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA" //spl token program
            
            if(transaction_program_id == token_program_id){
                arr[i].instructions[j].accounts.forEach(i=>console.log(i.toBase58()))
                var current_holder_account = arr[i].instructions[j].parsed.info
                // console.log(current_holder_account)
                holder = {
                    "holder_account": current_holder_account.destination || current_holder_account.account,
                    "owner": current_holder_account.owner,
                    "signature": signature
                }
                found = true
                break       
            }
            
        }
    }
    // console.log(holder)
    if(holder.owner == undefined && holder.holder_account == undefined){
        console.log("I have no owner and account")
        holder.owner = await acc.getDatabyNFT_addr(nft_addr)
    }
    
    if(holder.owner == undefined){
        console.log("I have no owner")
        holder.owner = await acc.getOwnerbyAccount(nft_addr, holder.holder_account)
        
    }
    if(holder.holder_account == undefined){
        console.log("I have no holder account")
        holder.holder_account = await acc.getAccountbyOwner(nft_addr, holder.owner)
    }
    return holder
}


  
   
async function holder_helper_func3(transaction_data, nft_addr,signature,instruction_arr){
    var holder = {
        "holder_account": undefined,
        "owner": undefined
    }
    for(var i =0; i < instruction_arr.length; i ++){
        
            // console.log("here", instruction_arr[i])
            var account = undefined
            if (instruction_arr[i].parsed.info.account != undefined || instruction_arr[i].parsed.info.destination != undefined ||instruction_arr[i].parsed.info.newAccount != undefined)
                account = instruction_arr[i].parsed.info.account|| instruction_arr[i].parsed.info.destination || instruction_arr[i].parsed.info.newAccount
            var owner = undefined
            if (instruction_arr[i].parsed.info.wallet != undefined)
                owner = instruction_arr[i].parsed.info.wallet
            if(account != undefined && owner!=undefined){
                // console.log ("and: ", instruction_arr[i].parsed.info.account && instruction_arr[i].parsed.info.wallet)
                holder = {
                    "holder_account": account,
                    "owner": owner,
                    "signature": signature
                }
                break
            }
            // console.log("owner", owner, "account", account)

            if (account == undefined && owner == undefined){
                if (instruction_arr[i].parsed.info.owner != undefined)
                    owner = instruction_arr[i].parsed.info.owner
            }
            if (account == undefined){
                account = await acc.getAccountbyOwner(nft_addr, owner)
            }
            if (owner == undefined){
                owner = await acc.getOwnerbyAccount(nft_addr, account)
            }
            // console.log("owner", owner, "account", account)
            if(owner != undefined && account != undefined){
                holder = {
                    "holder_account": account,
                    "owner": owner,
                    "signature": signature
                }
                break
            }
        }
        
    
    return holder
}

function check_innerInsturcution_function(instruction_arr){
    var tmp = []
    for (var k = 0 ; k < instruction_arr.length;k++){
        for (var l = 0; l < instruction_arr[k].instructions.length;l ++){
            // console.log("This is instruction",instruction_arr[k].instructions[l],instruction_arr[k].instructions[l].programId.toBase58())
            if(instruction_arr[k].instructions[l].parsed !=undefined){
                // console.log(instruction_arr[k].instructions[l], instruction_arr[k].instructions[l].programId.toBase58())
                tmp.push(instruction_arr[k].instructions[l])
            }
        }
            
    }
    return tmp
}

async function getCurrentHolder(nft_addr) {
    var count = 0 

    var instruction_arr = []
    var signature_arr = await connection.getSignaturesForAddress(new web3.PublicKey(nft_addr), {limit : 10}) //,  longest time to load
    while (count < 10){
        var signature = signature_arr[count].signature
        var transaction_data = await connection.getParsedConfirmedTransaction(signature) //longest time to load
        // console.log(transaction_data.meta.innerInstructions, transaction_data.meta.innerInstructions.length)
        if (transaction_data.meta.innerInstructions.length < 1)
            count = count + 1
        else{
            instruction_arr = transaction_data.meta.innerInstructions 
            instruction_arr = check_innerInsturcution_function(instruction_arr)
            if(instruction_arr < 1){
                count = count + 1
            }
            else{
            
                var holder = await holder_helper_func3(transaction_data, nft_addr,signature,instruction_arr)
                if (holder.owner != undefined && holder.holder_account != undefined){ 
                    
                    return holder
                }
              
                count = count + 1
                
            }
        }
    }
    
    
    // for (var k = 0 ; k < instruction_arr.length;k++){
    //     console.log("Instruction", instruction_arr[k])
    // }
    
    // var holder = holder_helper_func3(transaction_data, nft_addr,signature,instruction_arr)
    // // var holder = await holder_helper_function(transaction_data, nft_addr,signature)
    // return holder


    
}
module.exports = {getAllTokens,getCurrentHolder}