
const web3 = require('@solana/web3.js');
const bs58 = require('bs58')
const BN = require('bn.js');
const splToken = require('@solana/spl-token');
const { PublicKey, Connection } = require("@solana/web3.js/lib/index.cjs");
const axios = require("axios")
require("dotenv/config")

const mt = require('@metaplex/js');
const { Metadata } = require ("@metaplex-foundation/mpl-token-metadata");
const acc = require("./acc.js")
var connection = new mt.Connection(process.env.Cluster_Name)


class getNFTMetaData{ 
    constructor(NFT_addr) {
        return (async () => {
            
            var metadataAccount = await Metadata.getPDA(NFT_addr)
            
            var account = await connection.getParsedAccountInfo(metadataAccount)
            if(account.value==null){
                return null
            }
            else{
                const ownedMetadata = await Metadata.load(connection, metadataAccount);
                
                this.metadata = ownedMetadata
                
                return this
            }
        })();
    }
    get NFT_Name(){
        return this.metadata.data.data.name
    }
    get token_programID(){
        return this.metadata.pubkey.toBase58()//solscan-> transaction (createAccount)
    }
    
    get candyMachine_programID(){
        return this.metadata.info.owner.toBase58()
    }
    get getJsonUrl(){
        return this.metadata.data.data.uri
    }
    get getupdateAuth(){
        return this.metadata.data.updateAuthority
    }
    async imageUrl(){
 
        const jsonData = await axios.get(this.getJsonUrl)
        return jsonData.data.image
    }
    
}




async function updateAccountChange(old_signature){ 
    await connection.onAccountChange
    var test = await connection.onSignature(old_signature)
    console.log(test)
}

async function getDatabyProgram(program_acc){ //A7p8451ktDCHq5yYaHczeLMYsjRsAkzc3hCXcSrwYHU7 , NFT = 5U8gkhY66quoMn62ZyZ5kwfaKhJmZjAiBp47u6MvaM94
    var test = await connection.getProgramAccounts(new web3.PublicKey(program_acc))
    console.log(test.account)
}

module.exports = {getNFTMetaData}


