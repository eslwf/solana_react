const web3 = require('@solana/web3.js');
const bs58 = require('bs58')
const BN = require('bn.js');
const splToken = require('@solana/spl-token');
const { PublicKey, Connection } = require("@solana/web3.js/lib/index.cjs");
const axios = require("axios")
require("dotenv/config")
const mt = require('@metaplex/js');
const { Metadata } = require ("@metaplex-foundation/mpl-token-metadata");
var connection = new mt.Connection(process.env.Cluster_Name)
async function getTransactionHistory(nft_addr){
    var transaction_history = await connection.getConfirmedSignaturesForAddress2(new web3.PublicKey(nft_addr))
    return transaction_history
}

async function getDatafromParsedInstruction (instruction){
    var holder_account = instruction.info.destination || instruction.info.account
    return holder_account
} 

async function getDatafromPartiallyDecodedInstruction(instruction){
    // instruction.accounts.forEach(i=>console.log(i.toBase58()))
    var owner = instruction.accounts[1].toBase58() 
    //0 : signer
    //1 : dest
    //2 : NFT/ source
    //3 : NFT
    // console.log(owner)
    return owner
}


async function getLatestSignature(nft_addr){
    var list_of_signature = await connection.getSignaturesForAddress(new web3.PublicKey(nft_addr),{limit : 1})
    var signature = list_of_signature[0].signature
    return signature
}


async function checkTokenAccountTX (token_account){
    // var tx_history = await getTransactionHistory(token_account)
    // var latest_hist = tx_history[0]
    var signature = await connection.getSignaturesForAddress(new web3.PublicKey(token_account),{limit : 1})
    var tx_data = await connection.getParsedConfirmedTransaction(signature[0].signature) 
    return (tx_data.meta.innerInstructions[0].instructions[0].parsed.info.destination)
}
module.exports = {getTransactionHistory,getDatafromParsedInstruction,getDatafromPartiallyDecodedInstruction,getLatestSignature,checkTokenAccountTX}