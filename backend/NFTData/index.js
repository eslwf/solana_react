const web3 = require('@solana/web3.js');
const bs58 = require('bs58')
const BN = require('bn.js');
const splToken = require('@solana/spl-token');
const { PublicKey, Connection } = require("@solana/web3.js/lib/index.cjs");
const axios = require("axios")
require("dotenv/config")
const mt = require('@metaplex/js');
const { Metadata } = require ("@metaplex-foundation/mpl-token-metadata");
const fs = require("fs")
            /* Local Dependencies */
const acc = require("./acc.js")
const token = require("./tokens.js")
const tx = require("./tx.js")
const nft_class = require("./nft_class.js")

            /* Connection */
var connection = new mt.Connection(process.env.Cluster_Name)

async function get_json(json_url){
    try {
        var data_arr = await axios(json_url)
        return data_arr.data
    }
    catch(e){
        console.log(json_url)
    }
}

function trait_value_func (attr){
    var trait_arr = []
    var value_arr = []
    for(var i =0; i < attr.length;i++){
        // console.log("type of in nft_json attr", typeof(attr[i]["trait_type"]))
        trait_arr.push(attr[i]["trait_type"].toString())
        value_arr.push(attr[i]["value"].toString())
    }
    // console.log("trait_arr",trait_arr)
    return [trait_arr,value_arr]
}

       
    
async function oldmain(){
    /* Run on Multiple tokens*/
    var token_holding = await token.getAllTokens("chanvfyWDip63VmgxHdkpAXgt79PhiUcR4qDWdXvJLK") 
    
    // var filename =  "./testfile/6th.json"
    var count = 0;
    var no_meta = []
    // fs.appendFileSync(filename,"[", function(){})
    for(var i=0;i<token_holding.length;i++){ //
            var nft_addr = token_holding[i].nft_addr
            console.log("NFT: ", i, nft_addr) 
                if(nft_addr != "2hWrFTaGYXNLEVnTdvYPF5roxp4NSygoC8BYWYHYLuXd"){
                var holder = await token.getCurrentHolder(nft_addr)
                var distribution = await acc.checkAccountbyDistribution(holder.holder_account) //Token_Account
                var obj = Object.assign(token_holding[i] , holder) 
                obj.distribution = distribution;
                if(distribution == 0){
                    holder.holder_account = await tx.checkTokenAccountTX(holder.holder_account, holder.owner)
                    var new_dis = await acc.checkAccountbyDistribution(holder.holder_account) 
                    obj.distribution = new_dis
                }
                
                
        
                var meta = await new nft_class.getNFTMetaData(nft_addr)
                var get_data_from_url = await get_json(meta.getJsonUrl)
                try{
                    var attr = get_data_from_url["attributes"]
                    var trait_arr = trait_value_func(attr)[0]
                    var value_arr = trait_value_func(attr)[1]
    
                }
                catch(err){
                    console.log("error data " , data)
                    console.log(err.message)
                }
                if(meta!=null){
                    obj.nft_name = meta.NFT_Name
                    obj.img_url = await meta.imageUrl()
                    obj.json_url = meta.getJsonUrl
                    obj.trait_type = trait_arr
                    obj.value_arr = value_arr
                    console.log(obj, "\n")
                }
                else{
                    count++;
                    no_meta.push(holder)
                }
               
                
            
            // fs.appendFileSync(filename,JSON.stringify(obj,null, 4) , function(err){})
            // if(i!=token_holding.length-1)
            //     fs.appendFileSync(filename,",\n", function(err){})
            }
            
    }
    // fs.appendFileSync(filename,"]", function(){})
    console.log("Here is the count: ", count,no_meta)
}
// oldmain()

async function main(mint_addr)
{
    var token_holding = await token.getAllTokens(mint_addr) 
    var filename =  "./testfile/7th.json"
    fs.appendFileSync(filename,"[", function(){})
    for(var i=0;i<token_holding.length;i++){ 
        var nft_addr = token_holding[i].nft_addr
        console.log("NFT: ", i, nft_addr) 
            
                var holder = await token.getCurrentHolder(nft_addr)
                var obj = Object.assign(token_holding[i] , holder)
                var distribution = await acc.checkAccountbyDistribution(holder.holder_account)
                obj.distribution = distribution

                var meta = await new nft_class.getNFTMetaData(nft_addr)
                try{
                    var get_data_from_url = await get_json(meta.getJsonUrl)
                    var attr = get_data_from_url["attributes"]
                    var trait_arr = trait_value_func(attr)[0]
                    var value_arr = trait_value_func(attr)[1]
                    obj.nft_name = meta.NFT_Name
                    obj.img_url = await meta.imageUrl()
                    obj.json_url = meta.getJsonUrl
                    obj.trait_type = trait_arr
                    obj.value_arr = value_arr
                    console.log(obj, "\n")
                    fs.appendFileSync(filename,JSON.stringify(obj,null, 4) , function(err){})
                    if(i!=token_holding.length-1)
                        fs.appendFileSync(filename,",\n", function(err){})
    
                }
                catch(err){
                    console.log("error data " , nft_addr)
                    console.log(err.message)
                }
            
    }
    fs.appendFileSync(filename,"]", function(){}) 
}
// main("chanvfyWDip63VmgxHdkpAXgt79PhiUcR4qDWdXvJLK") 





async function singleNFT(){
    var token_holding = await token.getAllTokens("chanvfyWDip63VmgxHdkpAXgt79PhiUcR4qDWdXvJLK") 
    for(var i=0;i<token_holding.length;i++){ 
    if (token_holding[i].nft_addr == "9kaiMAzmD4u5VtCaf5UiZsXqjRgNHg2Y1nhxTFmA24XH"){
        var nft_addr = "9kaiMAzmD4u5VtCaf5UiZsXqjRgNHg2Y1nhxTFmA24XH"
    
        var holder = await token.getCurrentHolder(nft_addr)
        var obj = Object.assign(token_holding[i] , holder)
        var distribution = await acc.checkAccountbyDistribution(holder.holder_account)
        obj.distribution = distribution

        var meta = await new nft_class.getNFTMetaData(nft_addr)
        console.log(meta)
        
        try{
            var get_data_from_url = await get_json(meta.getJsonUrl)
            var attr = get_data_from_url["attributes"]
            var trait_arr = trait_value_func(attr)[0]
            var value_arr = trait_value_func(attr)[1]
            obj.nft_name = meta.NFT_Name
            obj.img_url = await meta.imageUrl()
            obj.json_url = meta.getJsonUrl
            obj.trait_type = trait_arr
            obj.value_arr = value_arr
            console.log(obj, "\n")

        }
        catch(err){
            console.log(err.message)
            var filename =  "./testfile/failed_metadata.json"
            fs.appendFileSync(filename,JSON.stringify(obj,null, 4) , function(err){})
        }
    }
    }
}
singleNFT()