const web3 = require('@solana/web3.js');
const bs58 = require('bs58')
const BN = require('bn.js');
const splToken = require('@solana/spl-token');
const { PublicKey, Connection } = require("@solana/web3.js/lib/index.cjs");
// const path = require('path');
const axios = require("axios")
require("dotenv/config")

const mt = require('@metaplex/js');
const { Metadata } = require ("@metaplex-foundation/mpl-token-metadata");
// const acc = require("./acc.js")
var connection = new mt.Connection(process.env.Cluster_Name)

async function checkAccountbyDistribution(nft_acc,nft_addr){ 
    var t = await connection.getTokenAccountBalance(new web3.PublicKey(nft_acc))
    // console.log(t)
    
    
    // var t2 = await connection.getTotalSupply(new web3.PublicKey(nft_addr))
    // console.log(t2)
    // var transaction_data = await connection.getParsedConfirmedTransaction(latest_transaction.signature) 

    // return transaction_data.meta.postTokenBalances[0].uiTokenAmount.amount
    return t.value.uiAmountString
}


async function getOwnerbyAccount(nft_addr,owner_account){
    // console.log(owner_account)
    var account_info = await connection.getParsedAccountInfo(new web3.PublicKey(owner_account))
    // var acc_info = splToken.AccountLayout.decode(account_info.value.data)
    // console.log("owner_account: ",new web3.PublicKey(acc_info.owner).toBase58())
    // console.log("owner program:",account_info.value.owner.toBase58() )
    // console.log("amount: ", splToken.u64.fromBuffer(acc_info.amount).toNumber())
    if ( account_info.value != null){
        if (account_info.value.data.parsed != undefined){
            var owner = account_info.value.data.parsed.info.owner
            return owner
        }
        else{
            return undefined
        }
    }
    else{
        return undefined
    }
}
async function getAccountbyOwner(nft_addr, owner){
    // console.log(owner)
    var token_account_info = await connection.getTokenAccountsByOwner(new web3.PublicKey(owner), {mint: new web3.PublicKey(nft_addr)})
    // console.log("token_account_info: ",token_account_info)
    
    var token_account = token_account_info.value[0].pubkey.toBase58()
    return token_account
}

async function getDatabyNFT_addr(nft_addr,owner_account){
    var account_info = await connection.getParsedAccountInfo(new web3.PublicKey(nft_addr))
    // console.log(account_info)
    return account_info.value.data.parsed.info.mintAuthority
}

module.exports = {checkAccountbyDistribution, getOwnerbyAccount, getOwnerbyAccount, getDatabyNFT_addr, getAccountbyOwner}